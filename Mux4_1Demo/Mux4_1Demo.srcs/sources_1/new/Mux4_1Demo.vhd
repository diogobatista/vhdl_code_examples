----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.10.2016 21:03:15
-- Design Name: 
-- Module Name: Mux4_1Demo - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Mux4_1Demo is
    Port ( sw : in STD_LOGIC_VECTOR (5 downto 0);
          led : out STD_LOGIC_VECTOR (0 downto 0));
end Mux4_1Demo;

architecture Shell of Mux4_1Demo is

begin
    Mux4_1 : entity work.Mux4_1 (Behavioral)
            port map (datain(0) => sw(0),
                    datain(1) => sw(1),
                    datain(2) => sw(2),
                    datain(3) => sw(3),
                    sel(0) => sw(4),
                    sel(1) => sw(5),
                    dataout => led(0));
                    

end Shell;
