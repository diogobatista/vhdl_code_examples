----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.10.2016 21:09:24
-- Design Name: 
-- Module Name: Mux4_1 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Mux4_1 is
    Port ( datain : in STD_LOGIC_VECTOR (3 downto 0);
           dataout : out STD_LOGIC;
           sel : in STD_LOGIC_VECTOR (1 downto 0));
end Mux4_1;

architecture Behavioral of Mux4_1 is

begin

    process( datain, sel)
    begin
        if(sel = "00") then
            dataout <= datain(0);
        elsif (sel = "01") then
            dataout <= datain(1);
        elsif (sel = "10") then
            dataout <= datain(2);
        else
            dataout <= datain(3);
        end if;
    end process;
            

end Behavioral;
