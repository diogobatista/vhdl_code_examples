----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.11.2016 20:22:23
-- Design Name: 
-- Module Name: TimerN_Demo - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity TimerN_Demo is
    Port ( btnC : in STD_LOGIC;
           sw : in STD_LOGIC_VECTOR (2 downto 0);
           led : out STD_LOGIC_VECTOR (0 downto 0));
end TimerN_Demo;

architecture Behavioral of TimerN_Demo is

begin



  TimerN : entity work.TimerN (Behavioral)
            port map (  start => sw (0),
                        enable => sw (1),
                        reset => sw (2),
                        timerOut => led(0),
                        clock => btnC);



end Behavioral;
