----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.11.2016 19:59:59
-- Design Name: 
-- Module Name: TimerN - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity TimerN is
    generic (N : positive := 6);
    Port ( start : in STD_LOGIC;
           reset : in STD_LOGIC;
           enable : in STD_LOGIC;
           clock : in STD_LOGIC;
           timerOut : out STD_LOGIC);
end TimerN;

architecture Behavioral of TimerN is

signal s_counter : unsigned(31 downto 0);
signal s_flag : std_logic;

begin

process(clock)
begin

    if (rising_edge(clock)) then
        if(start = '1') then
            s_flag <= '1';
        else
            null;
        end if;
        
        if(reset = '1') then
            s_counter <= (others => '0');
            s_flag <= '0';
            timerOut <= '0';
        elsif(enable = '0') then
            null;
        elsif(s_counter >= N) then
            timerOut <= '1';
        elsif(s_flag = '1') then
           s_counter <= s_counter + 1;
        else null;
        end if;
    end if;
end process;
end Behavioral;
