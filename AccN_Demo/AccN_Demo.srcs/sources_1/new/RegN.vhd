----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.11.2016 17:25:45
-- Design Name: 
-- Module Name: RegN - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity RegN is
    generic (N : integer := 8);
    Port ( dataIn : in STD_LOGIC_VECTOR ((N-1) downto 0);
           dataOut : out STD_LOGIC_VECTOR ((N-1) downto 0);
           reset : in STD_LOGIC;
           enable : in STD_LOGIC;
           clock : in STD_LOGIC);
end RegN;

architecture Behavioral of RegN is

begin

process(clock)
begin
        
    if (rising_edge(clock)) then    
        if(reset = '1') then
        dataOut <= (others => '0');
        elsif(enable = '0') then
        null;
        else
        dataOut <= dataIn;     
        end if;
    end if;
end process;

end Behavioral;
