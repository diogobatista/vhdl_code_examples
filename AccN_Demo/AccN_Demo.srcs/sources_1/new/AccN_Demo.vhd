----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.11.2016 18:06:15
-- Design Name: 
-- Module Name: AccN_Demo - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity AccN_Demo is
    generic (N : integer := 4);
    Port ( sw : in STD_LOGIC_VECTOR ((N+1) downto 0);
           led : out STD_LOGIC_VECTOR ((N-1) downto 0);
           btnC : in STD_LOGIC);
end AccN_Demo;

architecture Behavioral of AccN_Demo is

    signal s_adderOut : STD_LOGIC_VECTOR ((N-1) downto 0);
    signal s_accOut   : STD_LOGIC_VECTOR ((N-1) downto 0);

begin

      AdderN : entity work.AdderN (Behavioral)
                port map (  operand0 => sw ((N-1) downto 0),
                            operand1 => s_accOut,
                            result => s_adderOut);
                            
      RegN : entity work.RegN (Behavioral)
              port map ( dataIn => s_adderOut,
                         dataOut => s_accOut,
                         reset => sw(N),
                         enable => sw(N+1),
                         clock => btnC);     
                         
     led <= s_accOut;

end Behavioral;
