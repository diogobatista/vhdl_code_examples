library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;


entity RegN is
    generic (N : integer := 4);
    Port ( dataIn : in STD_LOGIC_VECTOR ((N-1) downto 0);
           dataOut : out STD_LOGIC_VECTOR ((N-1) downto 0);
           reset : in STD_LOGIC;
           enable : in STD_LOGIC;
           clock : in STD_LOGIC);
end RegN;

architecture Behavioral of RegN is

    signal s_dataout : unsigned ((N-1) downto 0);

begin

process(clock)
begin

    if(rising_edge(clock)) then
    
        if(reset = '1') then
            s_dataOut <= (others => '0');
        elsif(enable = '0') then
            null;
        else
            s_dataout <=  unsigned(dataIn);
        end if;
        
    end if;
end process;

dataOut <= std_logic_vector(s_dataOut);

end Behavioral;
