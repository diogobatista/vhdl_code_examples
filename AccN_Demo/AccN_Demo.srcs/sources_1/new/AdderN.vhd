----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 20.10.2016 15:12:30
-- Design Name: 
-- Module Name: AdderN - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity AdderN is
    generic (N : integer := 4);
    Port ( operand0 : in STD_LOGIC_VECTOR ((N-1) downto 0);
           operand1 : in STD_LOGIC_VECTOR ((N-1) downto 0);
           result : out STD_LOGIC_VECTOR ((N-1) downto 0));
end AdderN;

architecture Behavioral of AdderN is



begin

result <= std_logic_vector(unsigned(operand0) + unsigned(operand1));


end Behavioral;
