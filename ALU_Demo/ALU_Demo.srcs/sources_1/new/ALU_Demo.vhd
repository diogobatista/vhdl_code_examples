----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.11.2016 20:50:19
-- Design Name: 
-- Module Name: ALU_Demo - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ALU_Demo is
    generic (N : integer := 4);
    Port ( sw : in STD_LOGIC_VECTOR (10 downto 0);
           led : out STD_LOGIC_VECTOR (7 downto 0));
end ALU_Demo;

architecture Behavioral of ALU_Demo is

begin

  ALUN : entity work.ALUN (Behavioral)
            port map (  a => sw ((N-1) downto 0),
                        b => sw (((2*N)-1) downto (N)),
                        op => sw (((2*N)+2) downto (2*N)),
                        r => led ((N-1) downto 0),
                        m => led (((2*N)-1) downto (N)));

end Behavioral;
