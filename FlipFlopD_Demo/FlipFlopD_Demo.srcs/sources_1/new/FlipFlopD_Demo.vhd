----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.11.2016 12:57:45
-- Design Name: 
-- Module Name: FlipFlopD_Demo - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FlipFlopD_Demo is
    Port ( sw : in STD_LOGIC_VECTOR (2 downto 0);
           led : out STD_LOGIC_VECTOR (0 downto 0);
           clk : in STD_LOGIC);
end FlipFlopD_Demo;

architecture Behavioral of FlipFlopD_Demo is

begin

     flipflop : entity work.FlipFlopD (Behavioral)
            port map (  d => sw (0),
                        set => sw (1),
                        reset => sw (2),
                        q => led (0),
                        clk => clk);

end Behavioral;
