library IEEE;
use IEEE.STD_LOGIC_1164.all; 
use IEEE.NUMERIC_STD.all;  

entity CounterLoadUpDown4 is    
    port(   clock   : in  std_logic;         
            count : out std_logic_vector(3 downto 0);
            reset : in std_logic;
            upDown: in std_logic;
            dataIn: in std_logic_vector (3 downto 0);
            load  : in std_logic;
            enable: in std_logic;
            seg: out std_logic_vector (6 downto 0)); 
end CounterLoadUpDown4;  

architecture Behavioral of CounterLoadUpDown4 is    

signal s_count : unsigned(3 downto 0); 

begin    

process(clock)   
begin      

    
    if (rising_edge(clock)) then 
        if (reset = '1') then
                s_count <= "0000"; 
        elsif (enable = '0') then
                null;
        elsif (load = '1') then
            s_count <= unsigned(dataIn);
        elsif (upDown = '0') then
            s_count <= s_count - 1;     
        else
            s_count <= s_count + 1;
        end if;    
    end if;
    
end process;   

    Bin7SegDecoder : entity work.Bin7SegDecoder (Behavioral)
            port map (  binInput => std_logic_vector(s_count),
                        decOut_n (6 downto 0) => seg(6 downto 0));

count <= std_logic_vector(s_count); 
 
end Behavioral;