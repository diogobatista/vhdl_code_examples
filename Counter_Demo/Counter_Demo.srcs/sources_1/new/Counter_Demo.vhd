----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.11.2016 14:12:04
-- Design Name: 
-- Module Name: Counter_Demo - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;  

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Counter_Demo is
    Port ( led : out STD_LOGIC_VECTOR (3 downto 0);
           sw  : in STD_LOGIC_VECTOR (7 downto 0);
           btnC: in STD_LOGIC);
end Counter_Demo;

architecture Behavioral of Counter_Demo is


begin

    CounterLoadUpDown4 : entity work.CounterLoadUpDown4 (Behavioral)
            port map (  clock => btnC,
                        dataIn => sw(3 downto 0),
                        count (3 downto 0) => led (3 downto 0),
                        upDown => sw(4),
                        reset => sw (5),
                        load => sw(6),
                        enable => sw (7));
                            
end Behavioral;
