library IEEE;
use IEEE.STD_LOGIC_1164.all;  

entity Register8 is    
    port(   clk : in  std_logic;
            d   : in  std_logic_vector (7 downto 0);         
            q   : out std_logic_vector (7 downto 0);
            wrEn : in  std_logic); 
end Register8;  

architecture Behavioral of Register8 is 

begin    

process(clk)    
begin     
    if (rising_edge(clk)) then  
        if (wrEn = '0') then
            q <= "00000000";
        else
            q <= d;
        end if;
    end if;
end process; 
end Behavioral;