----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.11.2016 13:51:17
-- Design Name: 
-- Module Name: Register_Demo - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Register_Demo is
    Port ( sw : in STD_LOGIC_VECTOR (8 downto 0);
           led : out STD_LOGIC_VECTOR (7 downto 0);
           clk : in STD_LOGIC);
end Register_Demo;

architecture Behavioral of Register_Demo is

begin

     Register8 : entity work.Register8 (Behavioral)
            port map (  d => sw (7 downto 0),
                        wrEn => sw (8),
                        q => led (7 downto 0),
                        clk => clk);

end Behavioral;
