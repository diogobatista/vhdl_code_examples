----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 20.10.2016 13:39:44
-- Design Name: 
-- Module Name: shell - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity shell is
   generic(N : positive := 8);
   port(sw         : in  std_logic_vector((N-1) downto 0);
        led        : out std_logic_vector(3 downto 0));
end shell;

architecture Behavioral of shell is

begin

    CmpN: entity work.CmpN (Behavioral)
            port map (input0 => sw(3 downto 0),
                    input1 => sw(7 downto 4),
                    equal => led(0),
                    notEqual => led(1),
                    ltSigned => led(2),
                    ltUnsigned => led(3));

end Behavioral;
