----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06.10.2016 14:42:51
-- Design Name: 
-- Module Name: shell - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity shell is
    Port ( sw : in STD_LOGIC_VECTOR (8 downto 0);
           led : out STD_LOGIC_VECTOR (4 downto 0));
end shell;

architecture Behavioral of shell is

begin

    system_core : entity work.AddSub4(Behavioral)
            port map(   a => sw(3 downto 0),
                        b => sw(7 downto 4),
                        sub => sw(8),
                        s => led(3 downto 0),
                        cout => led(4));

end Behavioral;
