----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06.10.2016 15:12:51
-- Design Name: 
-- Module Name: AddSub4 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity AddSub4 is
    Port ( a : in STD_LOGIC_VECTOR (3 downto 0);
           b : in STD_LOGIC_VECTOR (3 downto 0);
           sub : in STD_LOGIC;
           s : out STD_LOGIC_VECTOR (3 downto 0);
           cout : out STD_LOGIC);
end AddSub4;

architecture Structural of AddSub4 is

signal s_b : std_logic_vector (3 downto 0);

begin

   adder4 : entity work.adder4(Behavioral)
        port map(  input0 => a,
                    input1 => s_b,
                    cin => sub,
                    cout => cout,
                    adder_output => s);
                    
    s_b <= b when (sub = '0') else not b; 
    

end Structural;


architecture Behavioral of AddSub4 is
    signal s_a, s_b, s_s: unsigned( 4 downto 0);
begin
    s_a <= '0' & unsigned(a);
    s_b <= '0' & unsigned(b);
    s_s <= (s_a + s_b) when (sub = '0') else
            (s_a - s_b);
    s <= std_logic_vector(s_s( 3 downto 0));
    cout <= s_s(4);
end Behavioral;