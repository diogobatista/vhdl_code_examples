----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06.10.2016 14:51:47
-- Design Name: 
-- Module Name: adder4 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity adder4 is
    Port ( input0 : in STD_LOGIC_VECTOR (3 downto 0);
           input1 : in STD_LOGIC_VECTOR (3 downto 0);
           cin : in STD_LOGIC;
           cout : out STD_LOGIC;
           adder_output : out STD_LOGIC_VECTOR (3 downto 0));
end adder4;

architecture Behavioral of adder4 is

    signal mem1, mem2, mem3 : std_logic;

begin

    bit0 : entity work.FullAdder(Behavioral)
        port map(  a => input0(0),
                    b => input1(0),
                    cin => cin,
                    cout => mem1,
                    s => adder_output(0));
                    
    bit1 : entity work.FullAdder(Behavioral)
        port map(  a => input0(1),
                    b => input1(1),
                    cin => mem1,
                    cout =>mem2,
                    s => adder_output(1));
                                
    bit2 : entity work.FullAdder(Behavioral)
        port map(  a => input0(2),
                    b => input1(2),
                    cin => mem2,
                    cout => mem3,
                    s => adder_output(2));
                                                
                                                
    bit3 : entity work.FullAdder(Behavioral)
        port map(  a => input0(3),
                    b => input1(3),
                    cin => mem3,
                    cout => cout,
                    s => adder_output(3));

end Behavioral;
