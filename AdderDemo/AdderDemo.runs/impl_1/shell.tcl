proc start_step { step } {
  set stopFile ".stop.rst"
  if {[file isfile .stop.rst]} {
    puts ""
    puts "*** Halting run - EA reset detected ***"
    puts ""
    puts ""
    return -code error
  }
  set beginFile ".$step.begin.rst"
  set platform "$::tcl_platform(platform)"
  set user "$::tcl_platform(user)"
  set pid [pid]
  set host ""
  if { [string equal $platform unix] } {
    if { [info exist ::env(HOSTNAME)] } {
      set host $::env(HOSTNAME)
    }
  } else {
    if { [info exist ::env(COMPUTERNAME)] } {
      set host $::env(COMPUTERNAME)
    }
  }
  set ch [open $beginFile w]
  puts $ch "<?xml version=\"1.0\"?>"
  puts $ch "<ProcessHandle Version=\"1\" Minor=\"0\">"
  puts $ch "    <Process Command=\".planAhead.\" Owner=\"$user\" Host=\"$host\" Pid=\"$pid\">"
  puts $ch "    </Process>"
  puts $ch "</ProcessHandle>"
  close $ch
}

proc end_step { step } {
  set endFile ".$step.end.rst"
  set ch [open $endFile w]
  close $ch
}

proc step_failed { step } {
  set endFile ".$step.error.rst"
  set ch [open $endFile w]
  close $ch
}

set_msg_config -id {HDL 9-1061} -limit 100000
set_msg_config -id {HDL 9-1654} -limit 100000

start_step init_design
set rc [catch {
  create_msg_db init_design.pb
  set_param xicom.use_bs_reader 1
  set_property design_mode GateLvl [current_fileset]
  set_param project.singleFileAddWarning.threshold 0
  set_property webtalk.parent_dir D:/SDA/AdderDemo/AdderDemo.cache/wt [current_project]
  set_property parent.project_path D:/SDA/AdderDemo/AdderDemo.xpr [current_project]
  set_property ip_repo_paths d:/SDA/AdderDemo/AdderDemo.cache/ip [current_project]
  set_property ip_output_repo d:/SDA/AdderDemo/AdderDemo.cache/ip [current_project]
  add_files -quiet D:/SDA/AdderDemo/AdderDemo.runs/synth_1/shell.dcp
  read_xdc D:/SDA/AdderDemo/AdderDemo.srcs/constrs_1/imports/SDA/Nexys4_Master.xdc
  link_design -top shell -part xc7a100tcsg324-1
  write_hwdef -file shell.hwdef
  close_msg_db -file init_design.pb
} RESULT]
if {$rc} {
  step_failed init_design
  return -code error $RESULT
} else {
  end_step init_design
}

start_step opt_design
set rc [catch {
  create_msg_db opt_design.pb
  opt_design 
  write_checkpoint -force shell_opt.dcp
  report_drc -file shell_drc_opted.rpt
  close_msg_db -file opt_design.pb
} RESULT]
if {$rc} {
  step_failed opt_design
  return -code error $RESULT
} else {
  end_step opt_design
}

start_step place_design
set rc [catch {
  create_msg_db place_design.pb
  implement_debug_core 
  place_design 
  write_checkpoint -force shell_placed.dcp
  report_io -file shell_io_placed.rpt
  report_utilization -file shell_utilization_placed.rpt -pb shell_utilization_placed.pb
  report_control_sets -verbose -file shell_control_sets_placed.rpt
  close_msg_db -file place_design.pb
} RESULT]
if {$rc} {
  step_failed place_design
  return -code error $RESULT
} else {
  end_step place_design
}

start_step route_design
set rc [catch {
  create_msg_db route_design.pb
  route_design 
  write_checkpoint -force shell_routed.dcp
  report_drc -file shell_drc_routed.rpt -pb shell_drc_routed.pb
  report_timing_summary -warn_on_violation -max_paths 10 -file shell_timing_summary_routed.rpt -rpx shell_timing_summary_routed.rpx
  report_power -file shell_power_routed.rpt -pb shell_power_summary_routed.pb -rpx shell_power_routed.rpx
  report_route_status -file shell_route_status.rpt -pb shell_route_status.pb
  report_clock_utilization -file shell_clock_utilization_routed.rpt
  close_msg_db -file route_design.pb
} RESULT]
if {$rc} {
  step_failed route_design
  return -code error $RESULT
} else {
  end_step route_design
}

start_step write_bitstream
set rc [catch {
  create_msg_db write_bitstream.pb
  catch { write_mem_info -force shell.mmi }
  write_bitstream -force shell.bit 
  catch { write_sysdef -hwdef shell.hwdef -bitfile shell.bit -meminfo shell.mmi -file shell.sysdef }
  catch {write_debug_probes -quiet -force debug_nets}
  close_msg_db -file write_bitstream.pb
} RESULT]
if {$rc} {
  step_failed write_bitstream
  return -code error $RESULT
} else {
  end_step write_bitstream
}

