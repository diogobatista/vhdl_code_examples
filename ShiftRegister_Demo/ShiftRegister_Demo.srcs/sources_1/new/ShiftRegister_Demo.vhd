----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.10.2016 14:00:17
-- Design Name: 
-- Module Name: ShiftRegister_Demo - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ShiftRegister_Demo is
    generic (size : positive := 8);
    Port ( sw : in STD_LOGIC_VECTOR (0 downto 0);
           btnC : in STD_LOGIC;
           led : out STD_LOGIC_VECTOR ((size-1) downto 0));
end ShiftRegister_Demo;

architecture Behavioral of ShiftRegister_Demo is

begin

    SR : entity work.ShiftRegisterN(Behavioral)
            port map(   clock => btnC,
                        sin => sw,
                        DataOut => led);


end Behavioral;
