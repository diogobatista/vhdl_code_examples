----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.10.2016 13:37:56
-- Design Name: 
-- Module Name: ShiftRegister4 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ShiftRegisterN is
    generic (N: positive := 8);
    Port ( clock : in STD_LOGIC;
           sin : in STD_LOGIC_VECTOR (0 downto 0);
           DataOut : out STD_LOGIC_VECTOR ((N-1) downto 0));
end ShiftRegisterN;

architecture Behavioral of ShiftRegisterN is

signal s_DataOut : std_logic_vector((N-1) downto 0);

begin

process (clock)
begin

if(rising_edge(clock)) then
s_DataOut <= s_DataOut((N-2) downto 0) & sin;
end if;

end process;

DataOut <= s_DataOut;

end Behavioral;
