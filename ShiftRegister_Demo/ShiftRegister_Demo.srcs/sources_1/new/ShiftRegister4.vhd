----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.10.2016 13:37:56
-- Design Name: 
-- Module Name: ShiftRegister4 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ShiftRegister4 is
    Port ( clock : in STD_LOGIC;
           sin : in STD_LOGIC_VECTOR (0 downto 0);
           DataOut : out STD_LOGIC_VECTOR (3 downto 0));
end ShiftRegister4;

architecture Behavioral of ShiftRegister4 is

signal s_DataOut : std_logic_vector(3 downto 0);

begin

process (clock)
begin

if(rising_edge(clock)) then
s_DataOut <= s_DataOut(2 downto 0) & sin;
end if;

end process;

DataOut <= s_DataOut;

end Behavioral;
