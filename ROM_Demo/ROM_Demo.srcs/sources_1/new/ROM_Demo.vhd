library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity ROM_Demo is
    Port ( sw : in STD_LOGIC_VECTOR (3 downto 0);
           led : out STD_LOGIC_VECTOR (7 downto 0));
end ROM_Demo;

architecture Behavioral of ROM_Demo is

begin

    ROM : entity work.ROM_16_8(RTL)
        port map( address => sw,
                  dataOut => led);

end Behavioral;
