library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity ROM_16_8 is
    port(address : in std_logic_vector(3 downto 0);
         dataOut : out std_logic_vector(7 downto 0));
end ROM_16_8;

architecture RTL of ROM_16_8 is

subtype TDataWord is std_logic_vector(7 downto 0);

type TROM is array (0 to 15) of TDataWord;

constant c_memory: TROM := ("00000001", "00111101", "00110010", "01001010",
"10110000", "10001111", "10111100", "01001101","00000001", "00111101", "00110010", "01001010",
"10110000", "10001111", "10111100", "01001101");

begin
dataOut <= c_memory(to_integer(unsigned(address)));
end RTL;