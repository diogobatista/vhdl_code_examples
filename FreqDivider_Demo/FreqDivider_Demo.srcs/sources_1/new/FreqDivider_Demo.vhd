----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.11.2016 16:38:13
-- Design Name: 
-- Module Name: FreqDivider_Demo - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FreqDivider_Demo is
    Port ( clk : in STD_LOGIC;
           led : out STD_LOGIC_VECTOR (0 downto 0));
end FreqDivider_Demo;

architecture Behavioral of FreqDivider_Demo is

begin

  FreqDivider : entity work.FreqDivider (Behavioral)
            port map (  clkIn => clk,
                        k => x"02FAF080",
                        clkOut => led(0));

end Behavioral;
