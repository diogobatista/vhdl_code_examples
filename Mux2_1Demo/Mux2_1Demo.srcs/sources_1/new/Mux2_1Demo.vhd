----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 29.09.2016 15:48:07
-- Design Name: 
-- Module Name: Mux2_1Demo - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Mux2_1Demo is
    Port ( sw : in STD_LOGIC_VECTOR (1 downto 0);
           led : out STD_LOGIC_VECTOR (0 downto 0);
           BTNC : in STD_LOGIC);
end Mux2_1Demo;

architecture Shell of Mux2_1Demo is

begin
    mux2_1 : entity work.Mux2_1(BehavioralBtn)
            port map(datain0 => sw(0),
                    datain1 => sw(1),
                    dataout => led (0),
                    sel => BTNC);
end Shell;
