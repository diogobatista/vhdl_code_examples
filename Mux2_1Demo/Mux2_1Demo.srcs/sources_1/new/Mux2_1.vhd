----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 29.09.2016 15:44:19
-- Design Name: 
-- Module Name: Mux2_1 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Mux2_1 is
    Port ( datain0 : in STD_LOGIC;
           datain1 : in STD_LOGIC;
           dataout : out STD_LOGIC;
           sel : in STD_LOGIC);
end Mux2_1;

architecture BehavioralBtn of Mux2_1 is

signal flag : std_logic;

begin

flag <= '0';

    process(sel, datain0, datain1)
    begin
        if(sel = '0' and flag = '0') then 
            dataout <=datain0;
            flag <= '1';
        elsif (sel = '0' and flag = '1') then
            dataout <= datain1;
            flag <= '0';
        else
            
        end if;
    end process;
end BehavioralBtn;


