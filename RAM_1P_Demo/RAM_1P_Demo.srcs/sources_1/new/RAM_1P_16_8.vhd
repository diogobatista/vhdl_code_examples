library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity RAM_1P_16_8 is
    Port ( clk : in STD_LOGIC;
           address : in STD_LOGIC_VECTOR (3 downto 0);
           writeEnable : in STD_LOGIC;
           writeData : in STD_LOGIC_VECTOR (7 downto 0);
           readData : out STD_LOGIC_VECTOR (7 downto 0));
end RAM_1P_16_8;

architecture Behavioral of RAM_1P_16_8 is

subtype TDataWord is std_logic_vector(7 downto 0);

type TROM is array (0 to 15) of TDataWord;

signal c_memory: TROM := ("00000001", "00111101", "00110010", "01001010",
"10110000", "10001111", "10111100", "01001101","00000001", "00111101", "00110010", "01001010",
"10110000", "10001111", "10111100", "01001101");


begin

process(clk)
begin
    if (rising_edge(clk)) then
        if (writeEnable = '1') then
           c_memory(to_integer(unsigned(address))) <= writeData;
        end if;
    end if;
end process;

readData <= c_memory(to_integer(unsigned(address)));

end Behavioral;
