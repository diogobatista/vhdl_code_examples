library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity ALU16_Tb is
end ALU16_Tb;


architecture Stimulus of ALU16_Tb is

 -- Sinais para ligar ?s entradas da UUT
 signal address : std_logic_vector(3 downto 0);
 signal writeEnable : std_logic;
 signal writeData : std_logic_vector(7 downto 0);
 signal clk : std_logic;
 
 -- Sinais para ligar ?s sa?das da UUT
 signal readData : std_logic_vector(7 downto 0);
 
begin

 uut: entity work.RAM_1P_16_8(Behavioral)
        port map(address => address,
                 writeEnable => writeEnable,
                 writeData => writeData,
                 readData => readData,
                 clk => clk);
                 
 --Process stim
 stim_proc : process
     begin
         clk <= '1';
         address <= x"C";
         writeEnable <= '1';
         writeData <= "00000101"; -- +
         wait for 100 ns;
         clk <= '0';
         wait for 100 ns;
         clk <= '1';
         address <= x"1";
         writeEnable <= '1';
         writeData <= "10011001"; -- +
         wait for 100 ns;
         clk <= '0';
         wait for 100 ns;
         clk <= '1';
         address <= x"C";
         writeEnable <= '0';
         writeData <= "00000000"; -- +
         wait for 100 ns;
         clk <= '0';
         wait for 100 ns;
         clk <= '1';
         address <= x"C";
         writeEnable <= '0';
         writeData <= "00000000"; -- +
         wait for 100 ns;
         wait;
     end process;
end Stimulus;