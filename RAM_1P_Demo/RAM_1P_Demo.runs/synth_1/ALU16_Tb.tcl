# 
# Synthesis run script generated by Vivado
# 

set_msg_config -id {HDL 9-1061} -limit 100000
set_msg_config -id {HDL 9-1654} -limit 100000
create_project -in_memory -part xc7a100tcsg324-1

set_param project.singleFileAddWarning.threshold 0
set_param project.compositeFile.enableAutoGeneration 0
set_param synth.vivado.isSynthRun true
set_property webtalk.parent_dir D:/SDA/RAM_1P_Demo/RAM_1P_Demo.cache/wt [current_project]
set_property parent.project_path D:/SDA/RAM_1P_Demo/RAM_1P_Demo.xpr [current_project]
set_property default_lib xil_defaultlib [current_project]
set_property target_language Verilog [current_project]
read_vhdl -library xil_defaultlib {
  D:/SDA/RAM_1P_Demo/RAM_1P_Demo.srcs/sources_1/new/RAM_1P_16_8.vhd
  D:/SDA/RAM_1P_Demo/RAM_1P_Demo.srcs/sources_1/new/RAM_1P_16_8_Tb.vhd
}
foreach dcp [get_files -quiet -all *.dcp] {
  set_property used_in_implementation false $dcp
}
read_xdc D:/SDA/RAM_1P_Demo/RAM_1P_Demo.srcs/constrs_1/imports/SDA/Nexys4_Master.xdc
set_property used_in_implementation false [get_files D:/SDA/RAM_1P_Demo/RAM_1P_Demo.srcs/constrs_1/imports/SDA/Nexys4_Master.xdc]


synth_design -top ALU16_Tb -part xc7a100tcsg324-1


write_checkpoint -force -noxdef ALU16_Tb.dcp

catch { report_utilization -file ALU16_Tb_utilization_synth.rpt -pb ALU16_Tb_utilization_synth.pb }
