@echo off
set xv_path=D:\\Programs\\Xilinx\\Vivado\\2016.2\\bin
call %xv_path%/xsim ALU16_Tb_behav -key {Behavioral:sim_1:Functional:ALU16_Tb} -tclbatch ALU16_Tb.tcl -log simulate.log
if "%errorlevel%"=="0" goto SUCCESS
if "%errorlevel%"=="1" goto END
:END
exit 1
:SUCCESS
exit 0
